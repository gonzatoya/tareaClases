/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dolares;

/**
 *
 * @author Personal
 */
public class Cambio {
    
    private double tipoCambio;
    private double colones;
    
    
    //metodo
    
    public double convertir(double colones){
        double caD = colones/tipoCambio;
        return Math.round(caD * 100)/100;
    }

    public double getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(double tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public double getColones() {
        return colones;
    }

    public void setColones(double colones) {
        this.colones = colones;
    }
    
    
    
}
