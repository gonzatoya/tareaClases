/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dolares;

import java.util.Scanner;

/**
 *
 * @author Personal
 */
public class Dolares {
    public static int leerInt(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        return Integer.parseInt(sc.nextLine());
    }

    public static double leerDouble(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        return Double.parseDouble(sc.nextLine());
    }

    public static String leerTexto(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        return sc.nextLine();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Cambio tC = new Cambio();
        
        String menu = "\n BANCO \n"
                + "1. Tipo de cambio $\n"
                + "2. Convertir a Dolares\n"
                + "3. Salir\n"
                + "Seleccione una opcion:";
        
        while(true){
            int op = leerInt(menu);
            if(op==3){
                break;
            }
            else if(op==1){
                tC.setTipoCambio(leerDouble("TIpo de Cambio"));
                
            }
            else if(op==2){
                tC.setColones(leerDouble("Cantidad de colones a convertir"));
                double total = tC.convertir(tC.getColones());
                System.out.println(tC.getColones() + "colones, equivale a " + total + " Dolares");
            }
        }
        
    }
    
    
}
