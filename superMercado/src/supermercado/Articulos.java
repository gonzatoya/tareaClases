/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package supermercado;

/**
 *
 * @author Personal
 */
public class Articulos {
    
    //atributos
    private int clave;
    private String descripcion;
    private double precio;
    private int cantidad;
    
    //get and set

    public int getClave() {
        return clave;
    }

    public void setClave(int clave) {
        this.clave = clave;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    
    //metodos
    public double impuesto(double precio){
        double precioIva = precio * 1.13;
        return Math.round(precioIva * 100) / 100;
    }
    
    //tostring

    @Override
    public String toString() {
        return "Articulos{" + "clave=" + clave + ", descripcion=" + descripcion + ", precio=" + precio + ", cantidad=" + cantidad + '}';
    }
    
    
    
    
    
}
