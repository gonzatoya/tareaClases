/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package supermercado;

import java.util.Objects;
import java.util.Scanner;

/**
 *
 * @author Personal
 */
public class SuperMercado {
    public static int leerInt(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        return Integer.parseInt(sc.nextLine());
    }

    public static double leerDouble(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        return Double.parseDouble(sc.nextLine());
    }

    public static String leerTexto(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        return sc.nextLine();
    }


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Articulos a1 = new Articulos();
        Articulos a2 = new Articulos();
        Articulos a3 = new Articulos();
        Articulos a4 = new Articulos();
        
        
        String menu = "\n   SUPERMERCADO  \n"
                + "1. Añadir articulo\n"
                + "2. Ver articulos\n"
                + "3. IVA\n"
                + "4. Salir\n"
                + "Seleccione una opción";
        
        String menuA = "\n      MENU ARTICULOS\n"
                + "1. Articulo 1\n"
                + "2. Articulo 2\n"
                + "3. Articulo 3\n"
                + "4. Articulo 4\n"
                + "5. Salir\n"
                + "Seleccione una opción";
        
        String menuI = "\n      OBTERNER IVA\n"
                + "1. Articulo 1\n"
                + "2. Articulo 2\n"
                + "3. Articulo 3\n"
                + "4. Articulo 4\n"
                + "5. Salir\n"
                + "Seleccione una opción";
        
        while(true){
            
            int op = leerInt(menu);
            
            if(op==4){
                break;
            }
            else if(op==1){
                while(true){
                    op = leerInt(menuA);
                    if(op==1){
                        a1.setClave(leerInt("clave"));
                        a1.setDescripcion(leerTexto("Descripcion"));
                        a1.setPrecio(leerDouble("Precio"));
                        a1.setCantidad(leerInt("Cantidad"));
                    }
                    else if(op==2){
                        a2.setClave(leerInt("clave"));
                        a2.setDescripcion(leerTexto("Descripcion"));
                        a2.setPrecio(leerDouble("Precio"));
                        a2.setCantidad(leerInt("Cantidad"));
                    }
                    else if(op==3){
                        a3.setClave(leerInt("clave"));
                        a3.setDescripcion(leerTexto("Descripcion"));
                        a3.setPrecio(leerDouble("Precio"));
                        a3.setCantidad(leerInt("Cantidad"));
                    }
                    else if(op==4){
                        a4.setClave(leerInt("clave"));
                        a4.setDescripcion(leerTexto("Descripcion"));
                        a4.setPrecio(leerDouble("Precio"));
                        a4.setCantidad(leerInt("Cantidad"));
                    }
                    else if(op==5){
                        break;
                    }
                    
                }
            }
            else if(op==2){
                System.out.println(Objects.toString(a1));
                System.out.println(Objects.toString(a2));
                System.out.println(Objects.toString(a3));
                System.out.println(Objects.toString(a4));
            }
            else if(op==3){
                while(true){
                    op = leerInt(menuI);
                    if(op==1){
                        double iva = a1.impuesto(a1.getPrecio());
                        System.out.println("Precio normal: "+ a1.getPrecio() + "Precio Iva: "+ iva);
                    }
                    else if(op==2){
                        double iva = a2.impuesto(a2.getPrecio());
                        System.out.println("Precio normal: "+ a2.getPrecio() + "Precio Iva: "+ iva);
                    }
                    else if(op==3){
                        double iva = a3.impuesto(a3.getPrecio());
                        System.out.println("Precio normal: "+ a3.getPrecio() + "Precio Iva: "+ iva);
                    }
                    else if(op==4){
                        double iva = a4.impuesto(a4.getPrecio());
                        System.out.println("Precio normal: "+ a4.getPrecio() + "Precio Iva: "+ iva);
                    }
                    else if(op==5){
                        break;
                    }
                    
                }
                
                
                
            }
        }
    }
    
}
