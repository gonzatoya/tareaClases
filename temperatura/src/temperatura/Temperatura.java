/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temperatura;

import java.util.Scanner;

/**
 *
 * @author Personal
 */
public class Temperatura {
    
    public static int leerInt(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        return Integer.parseInt(sc.nextLine());
    }

    public static double leerDouble(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        return Double.parseDouble(sc.nextLine());
    }

    public static String leerTexto(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        return sc.nextLine();
    }       
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Centigrados c1 = new Centigrados();
        
        String menu = "Convertidor de C a F\n"
                + "1. Guardar temperatura\n"
                + "2. Convertir a Fahrenheit\n"
                + "3. Salir\n"
                + "Seleccione una opcion:";
        
        while(true){
            int op = leerInt(menu);
            if(op==1){
                c1.setTemperatura(leerDouble("Temperatura:"));
                
            }
            else if(op==2){
                double caF = c1.getF(c1.getTemperatura());
                System.out.println(c1.getTemperatura() + "Equivale a " + caF + "grados Fahrenheit");
            }
            
        }
    }
    
}
